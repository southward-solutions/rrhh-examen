
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'exam1', component: () => import('pages/Exam1.vue') },
      { path: 'exam2', component: () => import('pages/Exam2.vue') },
      { path: 'exam3', component: () => import('pages/Exam3.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
